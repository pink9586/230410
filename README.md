# TTA GitPod VS Code


## Getting started

1. gitlab 가입
2. [TTA-CHAMP / SW Engineering / EA-20220829 · GitLab](https://gitlab.com/tta-champ/sw-engineering/ea-20220829) fork 주소

![fork.png](./fork.png)

3. gitlab 계정으로 gitpod.io 가입

![gitpod.png](./gitpod.png)

